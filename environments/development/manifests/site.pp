class { 'apache': }

apache::vhost { 'myMpwar.prod':
	port => '80',
	docroot => '/var/www/myproject/',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
}

apache::vhost { 'myMpwar.dev':
	port => '80',
	docroot => '/var/www/myproject/',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
}

class { '::mysql::server':
  root_password           => 'vagrantpass',
  remove_default_accounts => true,
  override_options        => $override_options
}

mysql::db { 'mympwar':
  user     => 'mympwaruser',
  password => 'mpwardb',
  host     => 'localhost',
  grant    => ['SELECT', 'UPDATE'],
}

mysql::db { 'mpwar_test':
  user     => 'mpwar_testuser',
  password => 'mpwardb',
  host     => 'localhost',
  grant    => ['SELECT', 'UPDATE'],
}

class { 'php': }

class { '::ntp':
  servers => [ '2.es.pool.ntp.org', '3.europe.pool.ntp.org', '2.europe.pool.ntp.org' ],
}

include moduling
